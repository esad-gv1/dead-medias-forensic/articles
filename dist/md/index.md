# Mémo ??

## Liste des épisodes

* [HyperCard - Afficher le menu](?mdpath=md/HyperCard-show_menu.md)
* [HyperCard - Débloquer une pile](?mdpath=md/HyperCard-unlock_stack.md)
* [HyperCard - Changer le type et le créateur](?mdpath=md/HyperCard-change_type_creator.md)


L'ensemble du projet est librement accessible ici :
https://gitlab.com/esad-gv1/memosjs.

Ceci concerne aussi le "moteur" qui permet d'utiliser les codes sources présentés dans les fichiers markdown afin de les injecter et de les exécuter dans une iFrame. L'idée de ce développement "fait maison" est de ne pas créer de dépendance avec une plateforme comme [codepen](https://codepen.io/), qui, malgré toutes leurs qualités, pourraient disparaître d'un moment à l'autre. Aussi, dans le contexte d'une consultation hors-ligne, l'ensemble des scripts nécessaires au bon fonctionnement du Mémo sont embarqués dans le projet lui-même, facilitant sa transmission sous la forme d'une archive zip, par exemple, pour une exécution en local.

Les composants logiciels suivants sont utilisés :

- markdown-it : [https://github.com/markdown-it/markdown-it](https://github.com/markdown-it/markdown-it)
- markdown-it-attrs.browser : [https://github.com/arve0/markdown-it-attrs](https://github.com/arve0/markdown-it-attrs)
- hightlightjs : [https://github.com/highlightjs/highlight.js](https://github.com/highlightjs/highlight.js)

Que leurs auteurs soient ici chaleureusement remerciés.

