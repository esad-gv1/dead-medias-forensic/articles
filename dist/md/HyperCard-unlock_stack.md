[Retour à l'index](index.html)

# Dead Medias Forensics

## HyperCard

Les fichiers HyperCard, des "*stack*" (piles), sont lisibles par l'application HyperCard elle-même. Cependant, diverses stratégies de protection des contenus étaient mises en œuvre pour bloquer l'accès aux sources.

Voici quelques "astuces" qui permettent d'ouvrir les piles provenant des CD-ROMs.

### Débloquer un fichier protégé par HyperTalk

Lors de la diffusion d'un CD-ROM, les auteurs et éditeurs peuvent choisir de bloquer l'accès au contenu du fichier à l'aide de certaines fonctions intégrées dans HyperCard, et appelées à partir de son langage de script : HyperTalk.

Il est possible d'utilise HyperCard lui-même pour retirer ces limitations d'accès, comme le propose cette *stack* produite par des hackers : [DeprotectHypercard](https://www.spacerogue.net/whacked/filelists/hacks.html).

Une fois la *stack* disponible dans le Mac OS 9 (ou antérieur), il faut l'ouvrir avec HyperCard et suivre les instructions affichées.


![](./assets/img/hypercard/deprotect0.png)

![](./assets/img/hypercard/deprotect1.png)

![](./assets/img/hypercard/deprotect2.png)

Une fois cette étape passée, une ouverture de la stack protégée peut être tentée en l'ouvrant depuis HyperCard (résultat non garanti!).

Voici le script attaché au bouton "Deprotect" :

![](./assets/img/hypercard/deprotect3.png)

```
on mouseUp

 put filename("STAK") into fname

 if fname is empty then exit mouseUp

 deprotect fname

 if the result is empty

 then answer "Deprotect Successful" with "OK"

 else if the result is -49

 then answer "Sorry, that file is busy."

 else if the result is -44

 then answer "Sorry, the disk is locked."

 else answer "Failed: unexpected file system error: " & the result with "OK"

end mouseUp
```

[Retour à l'index](index.html)