const fs = require('fs');
const archiver = require('archiver');

/**
 * @param {String} source
 * @param {String} out
 * @returns {Promise}
 */
function zipDirectory(source, out) {

    //erase .DS_Store file
    const ds_storePath = source + "/.DS_Store";

    if (fs.existsSync(ds_storePath)) {

        fs.unlinkSync(ds_storePath, function (err) {
            if (err) {
                console.log(err);
            }
            else {
                // if no error, file has been deleted successfully
                console.log('File deleted!');
            }
        });
    }

    const archive = archiver('zip', { zlib: { level: 9 } });
    const stream = fs.createWriteStream(out);

    return new Promise((resolve, reject) => {
        archive
            .directory(source, false)
            .on('error', err => reject(err))
            .pipe(stream)
            ;

        stream.on('close', () => resolve());
        archive.finalize();
    });
}

zipDirectory("./dist", "./dist/MemoJS.zip");
