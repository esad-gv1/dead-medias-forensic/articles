[Retour à l'index](index.html)

# Dead Medias Forensics

## HyperCard

Les fichiers HyperCard, des "stack" (piles), sont lisibles par l'application HyperCard elle-même. Cependant, diverses stratégies de protection des contenus étaient mises en œuvre pour bloquer l'accès aux sources.

Voici quelques "astuces" qui permettent d'ouvrir les piles provenant des CD-ROMs.

### Accéder au menu d'édition

Lorsque le CD-ROM est exécuté, il est le plus souvent en plein écran, ce qui masque le menu de l'application. Le faire apparaître à nouveau n'est pas toujours aisé. Parfois le fond de l'écran passe au noir, ce qui masque également le bureau du Mac OS, laissant peu de possibilités de passer d'une application à l'autre.

Une méthode consiste à faire faire apparaître la palette des outils dans un premier temps, à l'aide de la combinaison de touches `⎇ ⇥` (alt + tab), ci-dessous, un exemple avec un CD-ROM produit par Voyager Company :

Au lancement du CD-ROM, la barre de menu disparaît, empêchant de passer en mode "edition" dans HyperCard et de parcourir les sources :

![showmenu0.png](./assets/img/hypercard/showmenu0.png)

En faisant `⎇ ⇥` la palette d'outils apparaît :

![showmenu1.png](./assets/img/hypercard/showmenu1.png)    

En choisissant l'outil "Bouton", on passe en mode "édition" :

![showmenu2.png](./assets/img/hypercard/showmenu2.png)

En double cliquant sur l'un des boutons, on ouvre une fenêtre d'informations :

![showmenu3.png](./assets/img/hypercard/showmenu3.png)

En sélectionnant "Script", on ouvre la fenêtre d'édition du script attaché au bouton. Même si le script est vide, cette fenêtre s'ouvre : c'est à ce moment que le menu de HyperCard refait son apparition :

![showmenu4.png](./assets/img/hypercard/showmenu4.png)

L'ensemble de ce qui compose la "stack" est alors accessible.

![showmenu5.png](./assets/img/hypercard/showmenu5.png) 
![showmenu6.png](./assets/img/hypercard/showmenu6.png)


[Retour à l'index](index.html)
