import { MemoJS } from "@memojs/library";

console.log(MemoJS);

window.addEventListener("load", setup);

function setup() {
    const memo = new MemoJS('md/index.md');
    console.log(memo);
};