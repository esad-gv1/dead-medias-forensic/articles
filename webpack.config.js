const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = [
  {
    entry: './src/main.js',
    output: {
      filename: 'main.js',
      path: path.join(__dirname, 'dist', 'js'),
      libraryTarget: 'umd',
      library: 'main'
    },
    bail: true,
    devtool: 'source-map',
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env']
              }
            },
          ]
        },
        {
          test: /\.css$/i,
          use: ['style-loader', 'css-loader'],
        },
      ]
    },
    plugins: [
      new CopyPlugin({
        patterns: [
          { from: 'src/index.html', to: '../index.html' },
          { from: 'src/md', to: '../md' },
          { from: 'src/css', to: '../css' },
          { from: 'src/assets', to: '../assets' },
        ],
      }),
    ],
  },
]; // module exports
