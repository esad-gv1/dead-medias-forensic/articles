[Retour à l'index](index.html)

# Dead Medias Forensics

## HyperCard

Les fichiers *HyperCard*, des "stack" (piles), sont lisibles par l'application HyperCard elle-même. Cependant, diverses stratégies de protection des contenus étaient mises en œuvre pour bloquer l'accès aux sources.

Voici quelques "astuces" qui permettent d'ouvrir les piles provenant des CD-ROMs.

## Changer le type et le créateur d'un exécutable HyperCard

Lorsqu'une pile *HyperCard* est destinée à la publication sur un CD-ROM, une protection contre son ouverture en mode "édition" consiste à changer le type et le créateur de la pile.

Il s'agit de modifier la manière dont Mac OS 9 associe le fichier à une action à effectuer lorsqu'il est ouvert (par double click, par exemple) : est-ce q'un logiciel tiers doit tenter d'ouvrir le fichier ? est-ce qu'il doit être considéré comme étant en lecture seule (et donc ne pas être modifiable) ? doit-il être considéré comme étant une application ? etc.

Les piles *HyperCard* converties en application à l'aide de différentes "astuces" peuvent redevenir éditables après quelques modifications.

### Cas d'étude : Musicographie - 1995

#### Les outils

[FileBuddy](https://macintoshgarden.org/apps/file-buddy-348) est un outil qui permet d'analyser et de manipuler les fichiers sur Mac OS 9. Cet outil, qui partage certaines caractéristiques avec d'autres logiciels utilitaires comme [ResEdit](https://macintoshgarden.org/apps/resedit-30d1-ppc-copland-ready) ou [Resorcerer](https://macintoshgarden.org/apps/resorcerer-125), est particulièrement adapté à une ouverture rapide des dossiers et fichiers afin d'en observer les caractéristiques (visibilité du dossier, type et créateur du fichier, etc.).

![typecreator2.png](./assets/img/hypercard/typecreator2.png)

Une pile HyperCard éditable présente ces caractéristiques :

![typecreator0.png](./assets/img/hypercard/typecreator0.png)

Les "Finder Flags" peuvent permettre de rendre visibles des fichiers ou des dossiers qui ont été volontairement cachés par les auteurs.

On voit aussi la [signature](https://www.macdisk.com/macsigen.php) du type et du créateur du fichier, ici **STAK** et **WILD**.

Mais en ouvrant le fichier original de Musicographie, on découvre :

![typecreator4.png](./assets/img/hypercard/typecreator4.png)

Avec *FileBuddy*, il est possible de voir quels types sont disponibles pour le créateur concerné. Ici, on voit que le type **STAK** est disponible. Il s'agit donc de changer pour ce type, qui convertit alors le fichier en pile HyperCard!

![typecreator3.png](./assets/img/hypercard/typecreator3.png)



**NB** : Il est nécessaire d'enregistrer le fichier que les modifications apportées par *FileBuddy* (ou *ResEdit*). Il faut donc copier le fichier d'application depuis le CD-ROM vers le disque dur du Mac OS 9 et le modifier dans *FileBuddy* pour en créer une version "alternative".

Pour ouvrir pleinement le fichier, est possible de devoir [le débloquer depuis HyperCard lui-même](?mdpath=md/HyperCard-unlock_stack.md), ce qui permet, a terme, d'arriver à consulter les sources originales du programme!

![typecreator1.png](./assets/img/hypercard/typecreator1.png)

[Retour à l'index](index.html)
